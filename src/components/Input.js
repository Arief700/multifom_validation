import React from "react";
import PropTypes from "prop-types";

const Input = ({
  type = "text",
  placeHolder,
  name,
  value,
  onChange,
  error,
}) => {
  return (
    <div className="mb-5">
      <input
        className={error ? "input is-danger" : "input"}
        type={type}
        placeHolder={placeHolder}
        name={name}
        value={value}
        onChange={onChange}
        autoComplete="off"
      />
      {error && <div className="has-text-danger-dark">{error}</div>}
    </div>
  );
};

Input.propTypes = {
  type: PropTypes.string,
  placeHolder: PropTypes.string,
  name: PropTypes.string.isRequired,
  value: PropTypes.oneOfType([PropTypes.string, PropTypes.object]),
  onChange: PropTypes.func.isRequired,
};

export default Input;
