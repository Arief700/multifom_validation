import React, { Fragment } from "react";

import Form from "./components/Form";

const App = () => {
  return (
    <Fragment>
      <section className="hero is-light has-text-centered">
        <div className="hero-body">
          <div className="container">
            <h1 className="is-size-1">
              ReactJS multi step form with validation
            </h1>
          </div>
        </div>
      </section>
      <div className="container pt-5">
        <div className="columns">
          <div className="column is-half is-offset-one-quarter">
            {/* form component here */}
            <Form />
          </div>
        </div>
      </div>
    </Fragment>
  );
};

// source video : https://www.youtube.com/channel/UCS2UjgEPEybOx1toY7aKRJg

export default App;
